const CarInsurance = class {

  constructor(products = []) {
    this.products = products;

    this.maxPrice = 50;
    this.categories = this.setCategories();
    this.rules = this.setPricingRules();
  }

  setCategories(){
    return {
      'Low Coverage': {reduceSellIn: true, rules: ['downgradePrice','noNegativePrice']},
      'Medium Coverage': {reduceSellIn: true, rules: ['downgradePrice','noNegativePrice']},
      'Full Coverage': {reduceSellIn: true, rules: ['upgradePrice','topPrice']},
      'Mega Coverage': {reduceSellIn: false, rules: []},
      'Special Full Coverage': {reduceSellIn: true, rules: ['stepPricing','topPrice']},
      'Super Sale': {reduceSellIn: true, rules: ['downgradePriceSuper', 'noNegativePrice']}
    };
  }

  setPricingRules(){
    return {
      downgradePrice: (product) => {
        product.price -= product.sellIn < 0 ? 2 : 1;
      },
      downgradePriceSuper: (product) => {
        product.price -= product.sellIn < 0 ? 2 : 1;
      },
      noNegativePrice: (product) => {
        product.price = product.price < 0 ? 0 : product.price;
      },
      upgradePrice: (product) => {
        product.price += product.sellIn < 0 ? 2 : 1;
      },
      topPrice: (product) => {
        product.price = product.price < this.maxPrice ? product.price : this.maxPrice
      },
      stepPricing: (product) => {
        if (product.sellIn >= 10) product.price++;
        if (product.sellIn < 10 && product.sellIn >= 5) product.price += 2;
        if (product.sellIn < 5 && product.sellIn >= 0) product.price += 3;
        if (product.sellIn < 0) product.price = 0;
      }
    }
  }

  reduceSellIn(product){
    product.sellIn--;
  }

  updatePrice(){
    this.products.forEach((product) => {
      const selected = this.categories[product.name];

      if(selected.reduceSellIn){
        this.reduceSellIn(product);
      }

      selected.rules.forEach((rule) => {
        this.rules[rule](product);
      });

    });
    return this.products;
  }
}

module.exports = CarInsurance;
