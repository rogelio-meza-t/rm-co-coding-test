const assert = require('assert');
const expect = require('chai').expect;
const Product = require('../src/product.js');
const CarInsurance = require('../src/car-insurance.js');

describe('CarInsurance', ()=> {
  describe('Pricing Rules', () => {
    describe('Downgrade Price', ()=> {
      it('Decrease by 1', () => {
        const carInsurance = new CarInsurance([new Product('Low Coverage', 5, 7)]);
        const products = carInsurance.updatePrice();
        assert.equal(products[0].price, 6)
      })

      it('Decrease by 2', () => {
        const carInsurance = new CarInsurance([new Product('Low Coverage', -2, 7)]);
        const products = carInsurance.updatePrice();
        assert.equal(products[0].price, 5)
      })
    })

    describe('Downgrade Price Super', ()=> {
      it('Decrease by 1', () => {
        const carInsurance = new CarInsurance([new Product('Super Sale', 3, 6)]);
        const products = carInsurance.updatePrice();
        assert.equal(products[0].price, 5)
      })

      it('Decrease by 2', () => {
        const carInsurance = new CarInsurance([new Product('Super Sale', -2, 6)]);
        const products = carInsurance.updatePrice();
        assert.equal(products[0].price, 4)
      })
    })

    describe('No Negative Price', ()=> {
      it('Set the price to 0', () => {
        const carInsurance = new CarInsurance([new Product('Super Sale', 3, 0)]);
        let products = [];
        [1,2,3,4,5].forEach( (i) => {
          products = carInsurance.updatePrice();
        });
        assert.equal(products[0].price, 0)
      })
    })

    describe('Top Price', ()=> {
      it('Set the price to 50 as max', () => {
        const carInsurance = new CarInsurance([new Product('Special Full Coverage', 10, 49)]);
        let products = [];
        [1,2,3,4,5].forEach( (i) => {
          products = carInsurance.updatePrice();
        });
        assert.equal(products[0].price, 50)
      })
    })

    describe('Top Price', ()=> {
      it('Increase the price by 1', () => {
        const carInsurance = new CarInsurance([new Product('Special Full Coverage', 11, 24)]);
        const products = carInsurance.updatePrice();
        assert.equal(products[0].price, 25)
      })
      it('Increase the price by 2', () => {
        const carInsurance = new CarInsurance([new Product('Special Full Coverage', 10, 25)]);
        const products = carInsurance.updatePrice();
        assert.equal(products[0].price, 27)
      })
      it('Increase the price by 3', () => {
        const carInsurance = new CarInsurance([new Product('Special Full Coverage', 5, 35)]);
        const products = carInsurance.updatePrice();
        assert.equal(products[0].price, 38)
      })
    })
  })

  describe('Category Properties', () => {
    describe('Low Coverage', () => {
      it('Has downgradePrice and noNegativePrice rules', () => {
        const carInsurance = new CarInsurance();
        expect(carInsurance.categories['Low Coverage'].rules).to.eql(['downgradePrice','noNegativePrice']);
      })
    })

    describe('Medium Coverage', () => {
      it('Has downgradePrice and noNegativePrice rules', () => {
        const carInsurance = new CarInsurance();
        expect(carInsurance.categories['Medium Coverage'].rules).to.eql(['downgradePrice','noNegativePrice']);
      })
    })

    describe('Full Coverage', () => {
      it('Has upgradePrice and topPrice rules', () => {
        const carInsurance = new CarInsurance();
        expect(carInsurance.categories['Full Coverage'].rules).to.eql(['upgradePrice','topPrice']);
      })
    })

    describe('Mega Coverage', () => {
      it('Has no rules', () => {
        const carInsurance = new CarInsurance();
        expect(carInsurance.categories['Mega Coverage'].rules).to.eql([]);
      })
    })

    describe('Special Full Coverage', () => {
      it('Has stepPricing and topPrice rules', () => {
        const carInsurance = new CarInsurance();
        expect(carInsurance.categories['Special Full Coverage'].rules).to.eql(['stepPricing','topPrice']);
      })
    })

    describe('Super Sale', () => {
      it('Has downgradePriceSuper and noNegativePrice rules', () => {
        const carInsurance = new CarInsurance();
        expect(carInsurance.categories['Super Sale'].rules).to.eql(['downgradePriceSuper', 'noNegativePrice']);
      })
    })

  })
});
